#include <iostream>
#include <sys/types.h>
#include <assert.h>
#include <string.h>
#include <libusb.h>

#define GET_REPORT 0x01
#define SET_REPORT 0x09
#define GET_REPORT_REQ_TYPE 0xA1
#define SET_REPORT_REQ_TYPE 0x21

#pragma pack(push)
#pragma pack(1)
struct hidDescriptor_t {
    uint8_t  bLength;
    uint8_t  bHidDescriptorType;
    uint16_t bcdHID;
    uint8_t  bCountryCode;
    uint8_t  bNumDescriptors;
    uint8_t  bClassDescriptorType;
    uint16_t wDescriptorLength;
};
#pragma pack(pop)

struct color_t {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
    uint8_t buffer;
};

int main(int argc, char **argv) {
    int r;
    ssize_t cnt;

    libusb_device **deviceList;
    libusb_device *device;

    color_t newColor;

    if(argc < 4) {
        printf("Usage: %s <red> <green> <blue>\n", argv[0]);
        return 0;
    }

    newColor.red = atoi(argv[1]);
    newColor.green = atoi(argv[2]);
    newColor.blue = atoi(argv[3]);

    r = libusb_init(NULL);
    if (r < 0) {
        return r;
    }
    libusb_set_debug(NULL, 3);

    cnt = libusb_get_device_list(NULL, &deviceList);
    if (cnt < 0) {
        return (int)cnt;
    }


    for(int i = 0; i < cnt; i++) {
        libusb_device_descriptor descriptor;
        libusb_get_device_descriptor(deviceList[i], &descriptor);
        if (descriptor.idVendor == 0x03eb) {
            device = deviceList[i];
            break;
        }
    }

    if (device) {
        libusb_device_handle *deviceHandle;
        libusb_config_descriptor *configDescriptor;

        libusb_open(device, &deviceHandle);
        libusb_set_configuration(deviceHandle, 1);
        libusb_get_active_config_descriptor(device, &configDescriptor);
            
        libusb_interface intface = configDescriptor->interface[0];
        libusb_claim_interface(deviceHandle, 0);
        libusb_interface_descriptor intfaceDescriptor = intface.altsetting[0];
        hidDescriptor_t *hidDesc = (hidDescriptor_t*)intfaceDescriptor.extra;

        libusb_control_transfer(deviceHandle, SET_REPORT_REQ_TYPE, SET_REPORT, 0x0200, intfaceDescriptor.bInterfaceNumber, (unsigned char *)&newColor, 3, 10);

        libusb_release_interface(deviceHandle, 0);
        libusb_close(deviceHandle);
    }


    libusb_free_device_list(deviceList, 1);
    libusb_exit(NULL);
}